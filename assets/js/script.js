var modelViewer = document.querySelector('model-viewer');
var urlParams = new URLSearchParams(window.location.search);
var cabinType = urlParams.get('type') == null ? null : urlParams.get('type');
var cabinInfo = {
  "Deluxe-Cabin" : {
    Guest : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABsklEQVR4nN3VTYhPYRQG8N/4XiAfI18phZSFJJOFZOOjJAtsDJoFMUtZWNn4SCQWWIiUjZKpKYnEhkxTYlZiQULkY0KxkZ3eOup23Tv/9/a38tRbt+ec+z73nvec5+V/xkwcxkO8wH3siVgHurATmzCh6eaL8RpHsBxzsQqDeIwnGMAF9OEZejEqV+AOdtXEVuMTJhe4FbiFozmbT8RXjBkh5xq2lbhZeJ8jMB/PW+Scwb4Slz7oR47ADHxpkXMF3SVuDt7kCHREiTpHyBnC0hK3FTdzBMbhXXRSFcbiMxaU+EXRzi3RGV2S/qQOt7GhxF3CZZm4hxM1sR14iUkFLj0PY3yuwDR8wOiK2F2sLXGncU5D3MCBiiF7G+dQRJrmjU0FVoY1FHGqQjThPI43FegKgysibbK/pjEe4BF6cs5iCvpjYotIzvkUC2veWxPlSmXcXJWQev9s9PnJmi/ZjY+4HmdShWV4hfV/iOm4GhZ9sMUUC//vCdtO98USf2NdWLqp0deHYoqbYju+4ye+lVYaWBdxTHvYEv5UieG4Gve2sXrxC7OrBJIl/Ks1r81KNMdvh8lf6mX8oeAAAAAASUVORK5CYII=",
      value : 2
    },
    wifi : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABu0lEQVR4nO3UzYtOYRgG8B9mhM0wsxkrymfKYhSREoaVQrNC+BcwixGx8Bf42JHMAnuvr4XxEUVC1DTsNK+vFdmItR5dr05vE+/wzoqr7s5z7nPf17me69zP4T/+EnPQj0M4g/OJsj6IzamZFKZhB67hG+7jJIawPzGU3AN8xVVsT+8vsQVP8BR7MK8FQd2pLT2Ps6sJrbiMl01KOrEJJzCMG4nh5Daio2nnr3ARsxvkC/ECFyrJ+TiNj7gdS4rKDYmyPow7qTmF3orYIuB5uN1ELQ9n4Dg+4AjmtmBRqTmanmOYnnwt3HowGuKi9lLF+85MUVE4grHESD5yf8Wi7tg8Eq7RcP9Ab7wbzH1RsQ+vcRcHsBYLEusypvdSs7eifDBcDct+YmauXRnRh1jdgkVr8Cij2tXENSFu4Xpl68tjUbHmS2IsFi1LTUd6Su9vsRRvMRDi8dixKEQlFic3nhcNpKf0toSV+ISzmFXJ91Q/XJ6dS23pmRTKWWhMUpn5Oj4n6jkbHU21f4QanqGvkutL7oo2YD3eY1UlV+x4l99IW7A1hCuwBG+wTZuxM97Xs54S7MauqSL3b+I7Padh68CEfjkAAAAASUVORK5CYII=",
      value : 1
    },
    bedroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABXElEQVR4nO3VvUpcURTF8V8hgQSLRN8gL6ASbIOF2AgGBK0srOziI4SUNlaCIZ0yTdQuVaKFiOBHIQYrYZB0sRDsBhRE5MhSJqIphnshhRsWbM7Z+78OZ98PnuN/iRfow7uK1BfmfWxjFxsVaRdbd/CXOK/hVs7D9gpnNRichf2XQTdmsYp9/MIBdtDAdNTI2kFq9rCS3u5/GSxgDWPox9toEBOZ1Xbywbb9gfSshfGkQTnZePJevMcoJjGD79FM1kZTU2qlt/GUQRd+4BC/0cQmlvEV85iL5rO2nJpmeg7D6Hpo0MJRNj/gdQdDfZPen2G17gwK7CrDqyqmw7w9aJn8kupjCR9Lso6hGgyGwnaa+6s6evCnJJcYwXDFGgn7dhgbNelKHqe6otVuUN7GbzjGSYc6DqP3MYPPWGz7vnSqL/j0mEF59acquJbCKKx79nV+DhcVGhRWYV7fAHElj7ZEkHbpAAAAAElFTkSuQmCC",
      value : 1
    },
    bathroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABRUlEQVR4nL3VzyquURTH8Q/JSSTUS5lJ3IOBOmYMzpAS+ZOYkAFXYCCZnHKKGJwbOJlyA+QKlIkMDPxJ6Z0wRFvr1IPk7bX51aqnvZ/1++5n7/XsRV4t4Rw32EJjTvMxHKAzjHewmcu8D8f4gx8x1oTrHOYDsSWL2MNuYe4uB+AvTvEP+3hEc05AN6YifmGkMJcFUMJyRD8mcwOW0YMVtOM3GnIB6uKQa/ET9ehCby5AE2bieQItYT5UBKQKuP2ieEiAe7R+UdwnwFXUbjnjysvheSkOKP0gg/JpMP7q5P2sVFpzGQFz4enbAKuYzwhYwHpxYDwaRC5tY7Y40IazuFc+q47wSmX6QtM4/CSkhKNXF96btneCjQAOVxjTkZNyRytZRTnOZKfCSO+mnA+/PnWiC6xVsT2palLu/272rmqqMH839wnHLW6B2fuCHAAAAABJRU5ErkJggg==",
      value : 1
    },
    bathtub : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABJ0lEQVR4nO3WvUoDQRTF8V+w8qszoJ2giKXPIWKjAWMjsRDfQNBasRaMxsYHsNVCK3st1cZO7IJikQeQCSOEsBOzcbXyD7c5nD13uTM7s2Szh40M/RJzOfxJKjjP0I+wmcOfpIZ6hh5C1nL4k9xiqUsbRRPlPv1JJvGKoS49jOEihz/JDB4TY2jk8PfkDisZb/qC6T79PZnHdoa+jMUc/r9hKs53q+CqxbG2F+gEhwXXKR5CgyfsY6fgOvjaXbNo/UKDVtzCRuIXWjTNmP3f4PsRDeNN8bzF7DYfmCgwvIz3TqEeT8pSAeElnOG4UxzHDe6xi2o8/ys5qhqfDRnXGMvqvB4XpzFgNWNGchKr8c9hUK5S90K47p7jubTwgwbh2ZARstpX6CfYSWvMmK4jHQAAAABJRU5ErkJggg==",
      value : 0
    },
    living_Room : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABgElEQVR4nO3VT0tUYRQG8N/CNhWhSGmKSEMfINwJ2R9oF30ENyItJVeC2rYsw6X4KaxwUwhC+4TahSg4mI7mQoM+gJw4E9N4p3RmdvXAgfee8z7Pfe95zzmX//hn0Y1hPDijDSfnr+jDa+xhDatntLXkLON6I/Fr2MIsOpr46g48xSauFm1YwrzW8QqLRYFDdLbhBV34Vu+8gor2oZKav3Afx7jTBvG7qXWv6hjHBp7hE6ZaEJ/G59QKzbFwlnGjJn+VRlWAi7jcINaDffTmcwnbsn4jWMUCnuMh5vAOO/iRl7ef673sgai8R3hRV4W9ucckPuJWBgbxHW/xJHNZ1KVRcbcxkc15hIGMhdZ68n/iMVZqyI3S8CdcqlmvpOZvXfi15gStoB+7uFAfmM8x0Spm8bIocDMrKm6/WZRSI7QK8SEvuJyDb+scVk5uaBRiKCdhVMaXPE3pHBac4IZGaJ1C5C068T1Gm0hPcII7kz1xCjFDDrKmm/0fvEmNkar3BPoQYDKd3gEGAAAAAElFTkSuQmCC",
      value : 0
    },
    wardrobe : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+ElEQVR4nNXVoUtEQRDH8U8TLFaTh0kwahbfwQX/AZvBZDBpMguCVm2G+wcOrhru2pk0iCg8k0GQ64J/gByMcDwfD8FdPQd+sDvs7JfdmZ3lF63AWSIVdYANjLFXURejGv+n7nA+NR/HXl+shbLGv4PLhpP3sTU1L2OvvwG8Y1jRY0rACzo4xG2MT3Nc0SYGMU6egwU84R77OQBruI6gfg7AetR9FbCM1QZAG/M/ARzgpAEwitjZBbSw0gCYVN7cdwGTN7CbK8lLUabPOP53vajEIq5y9aIycvAQfaiTuhe94igWbYcuUgLe0KvoJhUg+5dZ5P70s9gHmNiR9c0iK9AAAAAASUVORK5CYII=",
      value : 1
    }, 
    terrace : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAB0ElEQVR4nO3Wu2tUURAG8B+awoCNoGARH41FwMYmoGDhqwskf0NsgpUiGIKgnQg+QLSw0SrY+GgVNaASm1Sm01VTCEpIYsBCwRQiI99KvNxCwpYZ+PbOznzzzZ4z9xyWdjuMVaw08CNoxldT899W5Mct8fNB04q70aD3W7SMZw18CJrx5fU0mMXxBu4GzXhxN4a8/iHvwUls7+FB2xHN3fWlgwd4uoawF2MtQkeCpo2lpmv1Zt3HO/m4ics9xi28lWW8xhSuYAETmMFDTOYyq9iTYCKxyXBmEluIxlQ0d3WX9BIHs/zu3t/DKLZiqWUGS8mNhtudRWkcwouu+JZcuf0Yx43EX2EIOzHf0mA+uaFwpXY8WivRdiJDKbuNU/HfYwD7MNfSYC65gXCltjTKnueUO4NLa6Z/NP439OFA9rjZYCa5vnCltvtjS/O0DOdCS4OvWer+3DXNBrPJ9YfbbHAR58o5hukEr+Fs/DcYzBZ0Whp0khsMV2qvxp/ubtEmfM5BGcajEOqgjORNWWxpsJjcSLhSOxytL9icuDt5hz/hJz5mX5fi/8qztqKwNlac4pZftaVRWnWt/7XrGci2HqG0SvOfBt9b/o6sF6X1p8FvmyW9fy6CvHMAAAAASUVORK5CYII=",
      value : 1
    }
  },
  "Suite-Cabin" : {
    Guest : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABsklEQVR4nN3VTYhPYRQG8N/4XiAfI18phZSFJJOFZOOjJAtsDJoFMUtZWNn4SCQWWIiUjZKpKYnEhkxTYlZiQULkY0KxkZ3eOup23Tv/9/a38tRbt+ec+z73nvec5+V/xkwcxkO8wH3siVgHurATmzCh6eaL8RpHsBxzsQqDeIwnGMAF9OEZejEqV+AOdtXEVuMTJhe4FbiFozmbT8RXjBkh5xq2lbhZeJ8jMB/PW+Scwb4Slz7oR47ADHxpkXMF3SVuDt7kCHREiTpHyBnC0hK3FTdzBMbhXXRSFcbiMxaU+EXRzi3RGV2S/qQOt7GhxF3CZZm4hxM1sR14iUkFLj0PY3yuwDR8wOiK2F2sLXGncU5D3MCBiiF7G+dQRJrmjU0FVoY1FHGqQjThPI43FegKgysibbK/pjEe4BF6cs5iCvpjYotIzvkUC2veWxPlSmXcXJWQev9s9PnJmi/ZjY+4HmdShWV4hfV/iOm4GhZ9sMUUC//vCdtO98USf2NdWLqp0deHYoqbYju+4ye+lVYaWBdxTHvYEv5UieG4Gve2sXrxC7OrBJIl/Ks1r81KNMdvh8lf6mX8oeAAAAAASUVORK5CYII=",
      value : 4
    },
    wifi : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABu0lEQVR4nO3UzYtOYRgG8B9mhM0wsxkrymfKYhSREoaVQrNC+BcwixGx8Bf42JHMAnuvr4XxEUVC1DTsNK+vFdmItR5dr05vE+/wzoqr7s5z7nPf17me69zP4T/+EnPQj0M4g/OJsj6IzamZFKZhB67hG+7jJIawPzGU3AN8xVVsT+8vsQVP8BR7MK8FQd2pLT2Ps6sJrbiMl01KOrEJJzCMG4nh5Daio2nnr3ARsxvkC/ECFyrJ+TiNj7gdS4rKDYmyPow7qTmF3orYIuB5uN1ELQ9n4Dg+4AjmtmBRqTmanmOYnnwt3HowGuKi9lLF+85MUVE4grHESD5yf8Wi7tg8Eq7RcP9Ab7wbzH1RsQ+vcRcHsBYLEusypvdSs7eifDBcDct+YmauXRnRh1jdgkVr8Cij2tXENSFu4Xpl68tjUbHmS2IsFi1LTUd6Su9vsRRvMRDi8dixKEQlFic3nhcNpKf0toSV+ISzmFXJ91Q/XJ6dS23pmRTKWWhMUpn5Oj4n6jkbHU21f4QanqGvkutL7oo2YD3eY1UlV+x4l99IW7A1hCuwBG+wTZuxM97Xs54S7MauqSL3b+I7Padh68CEfjkAAAAASUVORK5CYII=",
      value : 1
    },
    bedroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABXElEQVR4nO3VvUpcURTF8V8hgQSLRN8gL6ASbIOF2AgGBK0srOziI4SUNlaCIZ0yTdQuVaKFiOBHIQYrYZB0sRDsBhRE5MhSJqIphnshhRsWbM7Z+78OZ98PnuN/iRfow7uK1BfmfWxjFxsVaRdbd/CXOK/hVs7D9gpnNRichf2XQTdmsYp9/MIBdtDAdNTI2kFq9rCS3u5/GSxgDWPox9toEBOZ1Xbywbb9gfSshfGkQTnZePJevMcoJjGD79FM1kZTU2qlt/GUQRd+4BC/0cQmlvEV85iL5rO2nJpmeg7D6Hpo0MJRNj/gdQdDfZPen2G17gwK7CrDqyqmw7w9aJn8kupjCR9Lso6hGgyGwnaa+6s6evCnJJcYwXDFGgn7dhgbNelKHqe6otVuUN7GbzjGSYc6DqP3MYPPWGz7vnSqL/j0mEF59acquJbCKKx79nV+DhcVGhRWYV7fAHElj7ZEkHbpAAAAAElFTkSuQmCC",
      value : 1
    },
    bathroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABRUlEQVR4nL3VzyquURTH8Q/JSSTUS5lJ3IOBOmYMzpAS+ZOYkAFXYCCZnHKKGJwbOJlyA+QKlIkMDPxJ6Z0wRFvr1IPk7bX51aqnvZ/1++5n7/XsRV4t4Rw32EJjTvMxHKAzjHewmcu8D8f4gx8x1oTrHOYDsSWL2MNuYe4uB+AvTvEP+3hEc05AN6YifmGkMJcFUMJyRD8mcwOW0YMVtOM3GnIB6uKQa/ET9ehCby5AE2bieQItYT5UBKQKuP2ieEiAe7R+UdwnwFXUbjnjysvheSkOKP0gg/JpMP7q5P2sVFpzGQFz4enbAKuYzwhYwHpxYDwaRC5tY7Y40IazuFc+q47wSmX6QtM4/CSkhKNXF96btneCjQAOVxjTkZNyRytZRTnOZKfCSO+mnA+/PnWiC6xVsT2palLu/272rmqqMH839wnHLW6B2fuCHAAAAABJRU5ErkJggg==",
      value : 1
    },
    bathtub : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABJ0lEQVR4nO3WvUoDQRTF8V+w8qszoJ2giKXPIWKjAWMjsRDfQNBasRaMxsYHsNVCK3st1cZO7IJikQeQCSOEsBOzcbXyD7c5nD13uTM7s2Szh40M/RJzOfxJKjjP0I+wmcOfpIZ6hh5C1nL4k9xiqUsbRRPlPv1JJvGKoS49jOEihz/JDB4TY2jk8PfkDisZb/qC6T79PZnHdoa+jMUc/r9hKs53q+CqxbG2F+gEhwXXKR5CgyfsY6fgOvjaXbNo/UKDVtzCRuIXWjTNmP3f4PsRDeNN8bzF7DYfmCgwvIz3TqEeT8pSAeElnOG4UxzHDe6xi2o8/ys5qhqfDRnXGMvqvB4XpzFgNWNGchKr8c9hUK5S90K47p7jubTwgwbh2ZARstpX6CfYSWvMmK4jHQAAAABJRU5ErkJggg==",
      value : 0
    },
    living_Room : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABgElEQVR4nO3VT0tUYRQG8N/CNhWhSGmKSEMfINwJ2R9oF30ENyItJVeC2rYsw6X4KaxwUwhC+4TahSg4mI7mQoM+gJw4E9N4p3RmdvXAgfee8z7Pfe95zzmX//hn0Y1hPDijDSfnr+jDa+xhDatntLXkLON6I/Fr2MIsOpr46g48xSauFm1YwrzW8QqLRYFDdLbhBV34Vu+8gor2oZKav3Afx7jTBvG7qXWv6hjHBp7hE6ZaEJ/G59QKzbFwlnGjJn+VRlWAi7jcINaDffTmcwnbsn4jWMUCnuMh5vAOO/iRl7ef673sgai8R3hRV4W9ucckPuJWBgbxHW/xJHNZ1KVRcbcxkc15hIGMhdZ68n/iMVZqyI3S8CdcqlmvpOZvXfi15gStoB+7uFAfmM8x0Spm8bIocDMrKm6/WZRSI7QK8SEvuJyDb+scVk5uaBRiKCdhVMaXPE3pHBac4IZGaJ1C5C068T1Gm0hPcII7kz1xCjFDDrKmm/0fvEmNkar3BPoQYDKd3gEGAAAAAElFTkSuQmCC",
      value : 1
    },
    wardrobe : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+ElEQVR4nNXVoUtEQRDH8U8TLFaTh0kwahbfwQX/AZvBZDBpMguCVm2G+wcOrhru2pk0iCg8k0GQ64J/gByMcDwfD8FdPQd+sDvs7JfdmZ3lF63AWSIVdYANjLFXURejGv+n7nA+NR/HXl+shbLGv4PLhpP3sTU1L2OvvwG8Y1jRY0rACzo4xG2MT3Nc0SYGMU6egwU84R77OQBruI6gfg7AetR9FbCM1QZAG/M/ARzgpAEwitjZBbSw0gCYVN7cdwGTN7CbK8lLUabPOP53vajEIq5y9aIycvAQfaiTuhe94igWbYcuUgLe0KvoJhUg+5dZ5P70s9gHmNiR9c0iK9AAAAAASUVORK5CYII=",
      value : 1
    }, 
    terrace : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAB0ElEQVR4nO3Wu2tUURAG8B+awoCNoGARH41FwMYmoGDhqwskf0NsgpUiGIKgnQg+QLSw0SrY+GgVNaASm1Sm01VTCEpIYsBCwRQiI99KvNxCwpYZ+PbOznzzzZ4z9xyWdjuMVaw08CNoxldT899W5Mct8fNB04q70aD3W7SMZw18CJrx5fU0mMXxBu4GzXhxN4a8/iHvwUls7+FB2xHN3fWlgwd4uoawF2MtQkeCpo2lpmv1Zt3HO/m4ics9xi28lWW8xhSuYAETmMFDTOYyq9iTYCKxyXBmEluIxlQ0d3WX9BIHs/zu3t/DKLZiqWUGS8mNhtudRWkcwouu+JZcuf0Yx43EX2EIOzHf0mA+uaFwpXY8WivRdiJDKbuNU/HfYwD7MNfSYC65gXCltjTKnueUO4NLa6Z/NP439OFA9rjZYCa5vnCltvtjS/O0DOdCS4OvWer+3DXNBrPJ9YfbbHAR58o5hukEr+Fs/DcYzBZ0Whp0khsMV2qvxp/ubtEmfM5BGcajEOqgjORNWWxpsJjcSLhSOxytL9icuDt5hz/hJz5mX5fi/8qztqKwNlac4pZftaVRWnWt/7XrGci2HqG0SvOfBt9b/o6sF6X1p8FvmyW9fy6CvHMAAAAASUVORK5CYII=",
      value : 0
    }
  },
  "Family-Cabin" : {
    Guest : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABsklEQVR4nN3VTYhPYRQG8N/4XiAfI18phZSFJJOFZOOjJAtsDJoFMUtZWNn4SCQWWIiUjZKpKYnEhkxTYlZiQULkY0KxkZ3eOup23Tv/9/a38tRbt+ec+z73nvec5+V/xkwcxkO8wH3siVgHurATmzCh6eaL8RpHsBxzsQqDeIwnGMAF9OEZejEqV+AOdtXEVuMTJhe4FbiFozmbT8RXjBkh5xq2lbhZeJ8jMB/PW+Scwb4Slz7oR47ADHxpkXMF3SVuDt7kCHREiTpHyBnC0hK3FTdzBMbhXXRSFcbiMxaU+EXRzi3RGV2S/qQOt7GhxF3CZZm4hxM1sR14iUkFLj0PY3yuwDR8wOiK2F2sLXGncU5D3MCBiiF7G+dQRJrmjU0FVoY1FHGqQjThPI43FegKgysibbK/pjEe4BF6cs5iCvpjYotIzvkUC2veWxPlSmXcXJWQev9s9PnJmi/ZjY+4HmdShWV4hfV/iOm4GhZ9sMUUC//vCdtO98USf2NdWLqp0deHYoqbYju+4ye+lVYaWBdxTHvYEv5UieG4Gve2sXrxC7OrBJIl/Ks1r81KNMdvh8lf6mX8oeAAAAAASUVORK5CYII=",
      value : 5
    },
    wifi : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABu0lEQVR4nO3UzYtOYRgG8B9mhM0wsxkrymfKYhSREoaVQrNC+BcwixGx8Bf42JHMAnuvr4XxEUVC1DTsNK+vFdmItR5dr05vE+/wzoqr7s5z7nPf17me69zP4T/+EnPQj0M4g/OJsj6IzamZFKZhB67hG+7jJIawPzGU3AN8xVVsT+8vsQVP8BR7MK8FQd2pLT2Ps6sJrbiMl01KOrEJJzCMG4nh5Daio2nnr3ARsxvkC/ECFyrJ+TiNj7gdS4rKDYmyPow7qTmF3orYIuB5uN1ELQ9n4Dg+4AjmtmBRqTmanmOYnnwt3HowGuKi9lLF+85MUVE4grHESD5yf8Wi7tg8Eq7RcP9Ab7wbzH1RsQ+vcRcHsBYLEusypvdSs7eifDBcDct+YmauXRnRh1jdgkVr8Cij2tXENSFu4Xpl68tjUbHmS2IsFi1LTUd6Su9vsRRvMRDi8dixKEQlFic3nhcNpKf0toSV+ISzmFXJ91Q/XJ6dS23pmRTKWWhMUpn5Oj4n6jkbHU21f4QanqGvkutL7oo2YD3eY1UlV+x4l99IW7A1hCuwBG+wTZuxM97Xs54S7MauqSL3b+I7Padh68CEfjkAAAAASUVORK5CYII=",
      value : 1
    },
    bedroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABXElEQVR4nO3VvUpcURTF8V8hgQSLRN8gL6ASbIOF2AgGBK0srOziI4SUNlaCIZ0yTdQuVaKFiOBHIQYrYZB0sRDsBhRE5MhSJqIphnshhRsWbM7Z+78OZ98PnuN/iRfow7uK1BfmfWxjFxsVaRdbd/CXOK/hVs7D9gpnNRichf2XQTdmsYp9/MIBdtDAdNTI2kFq9rCS3u5/GSxgDWPox9toEBOZ1Xbywbb9gfSshfGkQTnZePJevMcoJjGD79FM1kZTU2qlt/GUQRd+4BC/0cQmlvEV85iL5rO2nJpmeg7D6Hpo0MJRNj/gdQdDfZPen2G17gwK7CrDqyqmw7w9aJn8kupjCR9Lso6hGgyGwnaa+6s6evCnJJcYwXDFGgn7dhgbNelKHqe6otVuUN7GbzjGSYc6DqP3MYPPWGz7vnSqL/j0mEF59acquJbCKKx79nV+DhcVGhRWYV7fAHElj7ZEkHbpAAAAAElFTkSuQmCC",
      value : 1
    },
    bathroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABRUlEQVR4nL3VzyquURTH8Q/JSSTUS5lJ3IOBOmYMzpAS+ZOYkAFXYCCZnHKKGJwbOJlyA+QKlIkMDPxJ6Z0wRFvr1IPk7bX51aqnvZ/1++5n7/XsRV4t4Rw32EJjTvMxHKAzjHewmcu8D8f4gx8x1oTrHOYDsSWL2MNuYe4uB+AvTvEP+3hEc05AN6YifmGkMJcFUMJyRD8mcwOW0YMVtOM3GnIB6uKQa/ET9ehCby5AE2bieQItYT5UBKQKuP2ieEiAe7R+UdwnwFXUbjnjysvheSkOKP0gg/JpMP7q5P2sVFpzGQFz4enbAKuYzwhYwHpxYDwaRC5tY7Y40IazuFc+q47wSmX6QtM4/CSkhKNXF96btneCjQAOVxjTkZNyRytZRTnOZKfCSO+mnA+/PnWiC6xVsT2palLu/272rmqqMH839wnHLW6B2fuCHAAAAABJRU5ErkJggg==",
      value : 1
    },
    bathtub : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABJ0lEQVR4nO3WvUoDQRTF8V+w8qszoJ2giKXPIWKjAWMjsRDfQNBasRaMxsYHsNVCK3st1cZO7IJikQeQCSOEsBOzcbXyD7c5nD13uTM7s2Szh40M/RJzOfxJKjjP0I+wmcOfpIZ6hh5C1nL4k9xiqUsbRRPlPv1JJvGKoS49jOEihz/JDB4TY2jk8PfkDisZb/qC6T79PZnHdoa+jMUc/r9hKs53q+CqxbG2F+gEhwXXKR5CgyfsY6fgOvjaXbNo/UKDVtzCRuIXWjTNmP3f4PsRDeNN8bzF7DYfmCgwvIz3TqEeT8pSAeElnOG4UxzHDe6xi2o8/ys5qhqfDRnXGMvqvB4XpzFgNWNGchKr8c9hUK5S90K47p7jubTwgwbh2ZARstpX6CfYSWvMmK4jHQAAAABJRU5ErkJggg==",
      value : 1
    },
    living_Room : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABgElEQVR4nO3VT0tUYRQG8N/CNhWhSGmKSEMfINwJ2R9oF30ENyItJVeC2rYsw6X4KaxwUwhC+4TahSg4mI7mQoM+gJw4E9N4p3RmdvXAgfee8z7Pfe95zzmX//hn0Y1hPDijDSfnr+jDa+xhDatntLXkLON6I/Fr2MIsOpr46g48xSauFm1YwrzW8QqLRYFDdLbhBV34Vu+8gor2oZKav3Afx7jTBvG7qXWv6hjHBp7hE6ZaEJ/G59QKzbFwlnGjJn+VRlWAi7jcINaDffTmcwnbsn4jWMUCnuMh5vAOO/iRl7ef673sgai8R3hRV4W9ucckPuJWBgbxHW/xJHNZ1KVRcbcxkc15hIGMhdZ68n/iMVZqyI3S8CdcqlmvpOZvXfi15gStoB+7uFAfmM8x0Spm8bIocDMrKm6/WZRSI7QK8SEvuJyDb+scVk5uaBRiKCdhVMaXPE3pHBac4IZGaJ1C5C068T1Gm0hPcII7kz1xCjFDDrKmm/0fvEmNkar3BPoQYDKd3gEGAAAAAElFTkSuQmCC",
      value : 1
    },
    wardrobe : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+ElEQVR4nNXVoUtEQRDH8U8TLFaTh0kwahbfwQX/AZvBZDBpMguCVm2G+wcOrhru2pk0iCg8k0GQ64J/gByMcDwfD8FdPQd+sDvs7JfdmZ3lF63AWSIVdYANjLFXURejGv+n7nA+NR/HXl+shbLGv4PLhpP3sTU1L2OvvwG8Y1jRY0rACzo4xG2MT3Nc0SYGMU6egwU84R77OQBruI6gfg7AetR9FbCM1QZAG/M/ARzgpAEwitjZBbSw0gCYVN7cdwGTN7CbK8lLUabPOP53vajEIq5y9aIycvAQfaiTuhe94igWbYcuUgLe0KvoJhUg+5dZ5P70s9gHmNiR9c0iK9AAAAAASUVORK5CYII=",
      value : 1
    }, 
    terrace : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAB0ElEQVR4nO3Wu2tUURAG8B+awoCNoGARH41FwMYmoGDhqwskf0NsgpUiGIKgnQg+QLSw0SrY+GgVNaASm1Sm01VTCEpIYsBCwRQiI99KvNxCwpYZ+PbOznzzzZ4z9xyWdjuMVaw08CNoxldT899W5Mct8fNB04q70aD3W7SMZw18CJrx5fU0mMXxBu4GzXhxN4a8/iHvwUls7+FB2xHN3fWlgwd4uoawF2MtQkeCpo2lpmv1Zt3HO/m4ics9xi28lWW8xhSuYAETmMFDTOYyq9iTYCKxyXBmEluIxlQ0d3WX9BIHs/zu3t/DKLZiqWUGS8mNhtudRWkcwouu+JZcuf0Yx43EX2EIOzHf0mA+uaFwpXY8WivRdiJDKbuNU/HfYwD7MNfSYC65gXCltjTKnueUO4NLa6Z/NP439OFA9rjZYCa5vnCltvtjS/O0DOdCS4OvWer+3DXNBrPJ9YfbbHAR58o5hukEr+Fs/DcYzBZ0Whp0khsMV2qvxp/ubtEmfM5BGcajEOqgjORNWWxpsJjcSLhSOxytL9icuDt5hz/hJz5mX5fi/8qztqKwNlac4pZftaVRWnWt/7XrGci2HqG0SvOfBt9b/o6sF6X1p8FvmyW9fy6CvHMAAAAASUVORK5CYII=",
      value : 1
    }
  },
  "Suite-Tent" : {
    Guest : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABsklEQVR4nN3VTYhPYRQG8N/4XiAfI18phZSFJJOFZOOjJAtsDJoFMUtZWNn4SCQWWIiUjZKpKYnEhkxTYlZiQULkY0KxkZ3eOup23Tv/9/a38tRbt+ec+z73nvec5+V/xkwcxkO8wH3siVgHurATmzCh6eaL8RpHsBxzsQqDeIwnGMAF9OEZejEqV+AOdtXEVuMTJhe4FbiFozmbT8RXjBkh5xq2lbhZeJ8jMB/PW+Scwb4Slz7oR47ADHxpkXMF3SVuDt7kCHREiTpHyBnC0hK3FTdzBMbhXXRSFcbiMxaU+EXRzi3RGV2S/qQOt7GhxF3CZZm4hxM1sR14iUkFLj0PY3yuwDR8wOiK2F2sLXGncU5D3MCBiiF7G+dQRJrmjU0FVoY1FHGqQjThPI43FegKgysibbK/pjEe4BF6cs5iCvpjYotIzvkUC2veWxPlSmXcXJWQev9s9PnJmi/ZjY+4HmdShWV4hfV/iOm4GhZ9sMUUC//vCdtO98USf2NdWLqp0deHYoqbYju+4ye+lVYaWBdxTHvYEv5UieG4Gve2sXrxC7OrBJIl/Ks1r81KNMdvh8lf6mX8oeAAAAAASUVORK5CYII=",
      value : 2
    },
    wifi : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABu0lEQVR4nO3UzYtOYRgG8B9mhM0wsxkrymfKYhSREoaVQrNC+BcwixGx8Bf42JHMAnuvr4XxEUVC1DTsNK+vFdmItR5dr05vE+/wzoqr7s5z7nPf17me69zP4T/+EnPQj0M4g/OJsj6IzamZFKZhB67hG+7jJIawPzGU3AN8xVVsT+8vsQVP8BR7MK8FQd2pLT2Ps6sJrbiMl01KOrEJJzCMG4nh5Daio2nnr3ARsxvkC/ECFyrJ+TiNj7gdS4rKDYmyPow7qTmF3orYIuB5uN1ELQ9n4Dg+4AjmtmBRqTmanmOYnnwt3HowGuKi9lLF+85MUVE4grHESD5yf8Wi7tg8Eq7RcP9Ab7wbzH1RsQ+vcRcHsBYLEusypvdSs7eifDBcDct+YmauXRnRh1jdgkVr8Cij2tXENSFu4Xpl68tjUbHmS2IsFi1LTUd6Su9vsRRvMRDi8dixKEQlFic3nhcNpKf0toSV+ISzmFXJ91Q/XJ6dS23pmRTKWWhMUpn5Oj4n6jkbHU21f4QanqGvkutL7oo2YD3eY1UlV+x4l99IW7A1hCuwBG+wTZuxM97Xs54S7MauqSL3b+I7Padh68CEfjkAAAAASUVORK5CYII=",
      value : 1
    },
    bedroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABXElEQVR4nO3VvUpcURTF8V8hgQSLRN8gL6ASbIOF2AgGBK0srOziI4SUNlaCIZ0yTdQuVaKFiOBHIQYrYZB0sRDsBhRE5MhSJqIphnshhRsWbM7Z+78OZ98PnuN/iRfow7uK1BfmfWxjFxsVaRdbd/CXOK/hVs7D9gpnNRichf2XQTdmsYp9/MIBdtDAdNTI2kFq9rCS3u5/GSxgDWPox9toEBOZ1Xbywbb9gfSshfGkQTnZePJevMcoJjGD79FM1kZTU2qlt/GUQRd+4BC/0cQmlvEV85iL5rO2nJpmeg7D6Hpo0MJRNj/gdQdDfZPen2G17gwK7CrDqyqmw7w9aJn8kupjCR9Lso6hGgyGwnaa+6s6evCnJJcYwXDFGgn7dhgbNelKHqe6otVuUN7GbzjGSYc6DqP3MYPPWGz7vnSqL/j0mEF59acquJbCKKx79nV+DhcVGhRWYV7fAHElj7ZEkHbpAAAAAElFTkSuQmCC",
      value : 1
    },
    bathroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABRUlEQVR4nL3VzyquURTH8Q/JSSTUS5lJ3IOBOmYMzpAS+ZOYkAFXYCCZnHKKGJwbOJlyA+QKlIkMDPxJ6Z0wRFvr1IPk7bX51aqnvZ/1++5n7/XsRV4t4Rw32EJjTvMxHKAzjHewmcu8D8f4gx8x1oTrHOYDsSWL2MNuYe4uB+AvTvEP+3hEc05AN6YifmGkMJcFUMJyRD8mcwOW0YMVtOM3GnIB6uKQa/ET9ehCby5AE2bieQItYT5UBKQKuP2ieEiAe7R+UdwnwFXUbjnjysvheSkOKP0gg/JpMP7q5P2sVFpzGQFz4enbAKuYzwhYwHpxYDwaRC5tY7Y40IazuFc+q47wSmX6QtM4/CSkhKNXF96btneCjQAOVxjTkZNyRytZRTnOZKfCSO+mnA+/PnWiC6xVsT2palLu/272rmqqMH839wnHLW6B2fuCHAAAAABJRU5ErkJggg==",
      value : 1
    },
    bathtub : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABJ0lEQVR4nO3WvUoDQRTF8V+w8qszoJ2giKXPIWKjAWMjsRDfQNBasRaMxsYHsNVCK3st1cZO7IJikQeQCSOEsBOzcbXyD7c5nD13uTM7s2Szh40M/RJzOfxJKjjP0I+wmcOfpIZ6hh5C1nL4k9xiqUsbRRPlPv1JJvGKoS49jOEihz/JDB4TY2jk8PfkDisZb/qC6T79PZnHdoa+jMUc/r9hKs53q+CqxbG2F+gEhwXXKR5CgyfsY6fgOvjaXbNo/UKDVtzCRuIXWjTNmP3f4PsRDeNN8bzF7DYfmCgwvIz3TqEeT8pSAeElnOG4UxzHDe6xi2o8/ys5qhqfDRnXGMvqvB4XpzFgNWNGchKr8c9hUK5S90K47p7jubTwgwbh2ZARstpX6CfYSWvMmK4jHQAAAABJRU5ErkJggg==",
      value : 0
    },
    living_Room : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABgElEQVR4nO3VT0tUYRQG8N/CNhWhSGmKSEMfINwJ2R9oF30ENyItJVeC2rYsw6X4KaxwUwhC+4TahSg4mI7mQoM+gJw4E9N4p3RmdvXAgfee8z7Pfe95zzmX//hn0Y1hPDijDSfnr+jDa+xhDatntLXkLON6I/Fr2MIsOpr46g48xSauFm1YwrzW8QqLRYFDdLbhBV34Vu+8gor2oZKav3Afx7jTBvG7qXWv6hjHBp7hE6ZaEJ/G59QKzbFwlnGjJn+VRlWAi7jcINaDffTmcwnbsn4jWMUCnuMh5vAOO/iRl7ef673sgai8R3hRV4W9ucckPuJWBgbxHW/xJHNZ1KVRcbcxkc15hIGMhdZ68n/iMVZqyI3S8CdcqlmvpOZvXfi15gStoB+7uFAfmM8x0Spm8bIocDMrKm6/WZRSI7QK8SEvuJyDb+scVk5uaBRiKCdhVMaXPE3pHBac4IZGaJ1C5C068T1Gm0hPcII7kz1xCjFDDrKmm/0fvEmNkar3BPoQYDKd3gEGAAAAAElFTkSuQmCC",
      value : 0
    },
    wardrobe : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+ElEQVR4nNXVoUtEQRDH8U8TLFaTh0kwahbfwQX/AZvBZDBpMguCVm2G+wcOrhru2pk0iCg8k0GQ64J/gByMcDwfD8FdPQd+sDvs7JfdmZ3lF63AWSIVdYANjLFXURejGv+n7nA+NR/HXl+shbLGv4PLhpP3sTU1L2OvvwG8Y1jRY0rACzo4xG2MT3Nc0SYGMU6egwU84R77OQBruI6gfg7AetR9FbCM1QZAG/M/ARzgpAEwitjZBbSw0gCYVN7cdwGTN7CbK8lLUabPOP53vajEIq5y9aIycvAQfaiTuhe94igWbYcuUgLe0KvoJhUg+5dZ5P70s9gHmNiR9c0iK9AAAAAASUVORK5CYII=",
      value : 0
    }, 
    terrace : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAB0ElEQVR4nO3Wu2tUURAG8B+awoCNoGARH41FwMYmoGDhqwskf0NsgpUiGIKgnQg+QLSw0SrY+GgVNaASm1Sm01VTCEpIYsBCwRQiI99KvNxCwpYZ+PbOznzzzZ4z9xyWdjuMVaw08CNoxldT899W5Mct8fNB04q70aD3W7SMZw18CJrx5fU0mMXxBu4GzXhxN4a8/iHvwUls7+FB2xHN3fWlgwd4uoawF2MtQkeCpo2lpmv1Zt3HO/m4ics9xi28lWW8xhSuYAETmMFDTOYyq9iTYCKxyXBmEluIxlQ0d3WX9BIHs/zu3t/DKLZiqWUGS8mNhtudRWkcwouu+JZcuf0Yx43EX2EIOzHf0mA+uaFwpXY8WivRdiJDKbuNU/HfYwD7MNfSYC65gXCltjTKnueUO4NLa6Z/NP439OFA9rjZYCa5vnCltvtjS/O0DOdCS4OvWer+3DXNBrPJ9YfbbHAR58o5hukEr+Fs/DcYzBZ0Whp0khsMV2qvxp/ubtEmfM5BGcajEOqgjORNWWxpsJjcSLhSOxytL9icuDt5hz/hJz5mX5fi/8qztqKwNlac4pZftaVRWnWt/7XrGci2HqG0SvOfBt9b/o6sF6X1p8FvmyW9fy6CvHMAAAAASUVORK5CYII=",
      value : 1
    }
  },
  "Deluxe-Tent" : {
    Guest : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABsklEQVR4nN3VTYhPYRQG8N/4XiAfI18phZSFJJOFZOOjJAtsDJoFMUtZWNn4SCQWWIiUjZKpKYnEhkxTYlZiQULkY0KxkZ3eOup23Tv/9/a38tRbt+ec+z73nvec5+V/xkwcxkO8wH3siVgHurATmzCh6eaL8RpHsBxzsQqDeIwnGMAF9OEZejEqV+AOdtXEVuMTJhe4FbiFozmbT8RXjBkh5xq2lbhZeJ8jMB/PW+Scwb4Slz7oR47ADHxpkXMF3SVuDt7kCHREiTpHyBnC0hK3FTdzBMbhXXRSFcbiMxaU+EXRzi3RGV2S/qQOt7GhxF3CZZm4hxM1sR14iUkFLj0PY3yuwDR8wOiK2F2sLXGncU5D3MCBiiF7G+dQRJrmjU0FVoY1FHGqQjThPI43FegKgysibbK/pjEe4BF6cs5iCvpjYotIzvkUC2veWxPlSmXcXJWQev9s9PnJmi/ZjY+4HmdShWV4hfV/iOm4GhZ9sMUUC//vCdtO98USf2NdWLqp0deHYoqbYju+4ye+lVYaWBdxTHvYEv5UieG4Gve2sXrxC7OrBJIl/Ks1r81KNMdvh8lf6mX8oeAAAAAASUVORK5CYII=",
      value : 4
    },
    wifi : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABu0lEQVR4nO3UzYtOYRgG8B9mhM0wsxkrymfKYhSREoaVQrNC+BcwixGx8Bf42JHMAnuvr4XxEUVC1DTsNK+vFdmItR5dr05vE+/wzoqr7s5z7nPf17me69zP4T/+EnPQj0M4g/OJsj6IzamZFKZhB67hG+7jJIawPzGU3AN8xVVsT+8vsQVP8BR7MK8FQd2pLT2Ps6sJrbiMl01KOrEJJzCMG4nh5Daio2nnr3ARsxvkC/ECFyrJ+TiNj7gdS4rKDYmyPow7qTmF3orYIuB5uN1ELQ9n4Dg+4AjmtmBRqTmanmOYnnwt3HowGuKi9lLF+85MUVE4grHESD5yf8Wi7tg8Eq7RcP9Ab7wbzH1RsQ+vcRcHsBYLEusypvdSs7eifDBcDct+YmauXRnRh1jdgkVr8Cij2tXENSFu4Xpl68tjUbHmS2IsFi1LTUd6Su9vsRRvMRDi8dixKEQlFic3nhcNpKf0toSV+ISzmFXJ91Q/XJ6dS23pmRTKWWhMUpn5Oj4n6jkbHU21f4QanqGvkutL7oo2YD3eY1UlV+x4l99IW7A1hCuwBG+wTZuxM97Xs54S7MauqSL3b+I7Padh68CEfjkAAAAASUVORK5CYII=",
      value : 1
    },
    bedroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABXElEQVR4nO3VvUpcURTF8V8hgQSLRN8gL6ASbIOF2AgGBK0srOziI4SUNlaCIZ0yTdQuVaKFiOBHIQYrYZB0sRDsBhRE5MhSJqIphnshhRsWbM7Z+78OZ98PnuN/iRfow7uK1BfmfWxjFxsVaRdbd/CXOK/hVs7D9gpnNRichf2XQTdmsYp9/MIBdtDAdNTI2kFq9rCS3u5/GSxgDWPox9toEBOZ1Xbywbb9gfSshfGkQTnZePJevMcoJjGD79FM1kZTU2qlt/GUQRd+4BC/0cQmlvEV85iL5rO2nJpmeg7D6Hpo0MJRNj/gdQdDfZPen2G17gwK7CrDqyqmw7w9aJn8kupjCR9Lso6hGgyGwnaa+6s6evCnJJcYwXDFGgn7dhgbNelKHqe6otVuUN7GbzjGSYc6DqP3MYPPWGz7vnSqL/j0mEF59acquJbCKKx79nV+DhcVGhRWYV7fAHElj7ZEkHbpAAAAAElFTkSuQmCC",
      value : 1
    },
    bathroom : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABRUlEQVR4nL3VzyquURTH8Q/JSSTUS5lJ3IOBOmYMzpAS+ZOYkAFXYCCZnHKKGJwbOJlyA+QKlIkMDPxJ6Z0wRFvr1IPk7bX51aqnvZ/1++5n7/XsRV4t4Rw32EJjTvMxHKAzjHewmcu8D8f4gx8x1oTrHOYDsSWL2MNuYe4uB+AvTvEP+3hEc05AN6YifmGkMJcFUMJyRD8mcwOW0YMVtOM3GnIB6uKQa/ET9ehCby5AE2bieQItYT5UBKQKuP2ieEiAe7R+UdwnwFXUbjnjysvheSkOKP0gg/JpMP7q5P2sVFpzGQFz4enbAKuYzwhYwHpxYDwaRC5tY7Y40IazuFc+q47wSmX6QtM4/CSkhKNXF96btneCjQAOVxjTkZNyRytZRTnOZKfCSO+mnA+/PnWiC6xVsT2palLu/272rmqqMH839wnHLW6B2fuCHAAAAABJRU5ErkJggg==",
      value : 1
    },
    bathtub : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABJ0lEQVR4nO3WvUoDQRTF8V+w8qszoJ2giKXPIWKjAWMjsRDfQNBasRaMxsYHsNVCK3st1cZO7IJikQeQCSOEsBOzcbXyD7c5nD13uTM7s2Szh40M/RJzOfxJKjjP0I+wmcOfpIZ6hh5C1nL4k9xiqUsbRRPlPv1JJvGKoS49jOEihz/JDB4TY2jk8PfkDisZb/qC6T79PZnHdoa+jMUc/r9hKs53q+CqxbG2F+gEhwXXKR5CgyfsY6fgOvjaXbNo/UKDVtzCRuIXWjTNmP3f4PsRDeNN8bzF7DYfmCgwvIz3TqEeT8pSAeElnOG4UxzHDe6xi2o8/ys5qhqfDRnXGMvqvB4XpzFgNWNGchKr8c9hUK5S90K47p7jubTwgwbh2ZARstpX6CfYSWvMmK4jHQAAAABJRU5ErkJggg==",
      value : 0
    },
    living_Room : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAABgElEQVR4nO3VT0tUYRQG8N/CNhWhSGmKSEMfINwJ2R9oF30ENyItJVeC2rYsw6X4KaxwUwhC+4TahSg4mI7mQoM+gJw4E9N4p3RmdvXAgfee8z7Pfe95zzmX//hn0Y1hPDijDSfnr+jDa+xhDatntLXkLON6I/Fr2MIsOpr46g48xSauFm1YwrzW8QqLRYFDdLbhBV34Vu+8gor2oZKav3Afx7jTBvG7qXWv6hjHBp7hE6ZaEJ/G59QKzbFwlnGjJn+VRlWAi7jcINaDffTmcwnbsn4jWMUCnuMh5vAOO/iRl7ef673sgai8R3hRV4W9ucckPuJWBgbxHW/xJHNZ1KVRcbcxkc15hIGMhdZ68n/iMVZqyI3S8CdcqlmvpOZvXfi15gStoB+7uFAfmM8x0Spm8bIocDMrKm6/WZRSI7QK8SEvuJyDb+scVk5uaBRiKCdhVMaXPE3pHBac4IZGaJ1C5C068T1Gm0hPcII7kz1xCjFDDrKmm/0fvEmNkar3BPoQYDKd3gEGAAAAAElFTkSuQmCC",
      value : 0
    },
    wardrobe : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAA+ElEQVR4nNXVoUtEQRDH8U8TLFaTh0kwahbfwQX/AZvBZDBpMguCVm2G+wcOrhru2pk0iCg8k0GQ64J/gByMcDwfD8FdPQd+sDvs7JfdmZ3lF63AWSIVdYANjLFXURejGv+n7nA+NR/HXl+shbLGv4PLhpP3sTU1L2OvvwG8Y1jRY0rACzo4xG2MT3Nc0SYGMU6egwU84R77OQBruI6gfg7AetR9FbCM1QZAG/M/ARzgpAEwitjZBbSw0gCYVN7cdwGTN7CbK8lLUabPOP53vajEIq5y9aIycvAQfaiTuhe94igWbYcuUgLe0KvoJhUg+5dZ5P70s9gHmNiR9c0iK9AAAAAASUVORK5CYII=",
      value : 1
    }, 
    terrace : {
      image : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAACXBIWXMAAAsTAAALEwEAmpwYAAAB0ElEQVR4nO3Wu2tUURAG8B+awoCNoGARH41FwMYmoGDhqwskf0NsgpUiGIKgnQg+QLSw0SrY+GgVNaASm1Sm01VTCEpIYsBCwRQiI99KvNxCwpYZ+PbOznzzzZ4z9xyWdjuMVaw08CNoxldT899W5Mct8fNB04q70aD3W7SMZw18CJrx5fU0mMXxBu4GzXhxN4a8/iHvwUls7+FB2xHN3fWlgwd4uoawF2MtQkeCpo2lpmv1Zt3HO/m4ics9xi28lWW8xhSuYAETmMFDTOYyq9iTYCKxyXBmEluIxlQ0d3WX9BIHs/zu3t/DKLZiqWUGS8mNhtudRWkcwouu+JZcuf0Yx43EX2EIOzHf0mA+uaFwpXY8WivRdiJDKbuNU/HfYwD7MNfSYC65gXCltjTKnueUO4NLa6Z/NP439OFA9rjZYCa5vnCltvtjS/O0DOdCS4OvWer+3DXNBrPJ9YfbbHAR58o5hukEr+Fs/DcYzBZ0Whp0khsMV2qvxp/ubtEmfM5BGcajEOqgjORNWWxpsJjcSLhSOxytL9icuDt5hz/hJz5mX5fi/8qztqKwNlac4pZftaVRWnWt/7XrGci2HqG0SvOfBt9b/o6sF6X1p8FvmyW9fy6CvHMAAAAASUVORK5CYII=",
      value : 1
    }
  }
};

// Handles loading the events for <model-viewer>'s slotted progress bar
const onProgress = (event) => {
  const progressBar = event.target.querySelector('.progress-bar');
  const updatingBar = event.target.querySelector('.update-bar');
  updatingBar.style.width = `${event.detail.totalProgress * 100}%`;
  if (event.detail.totalProgress === 1) {
    progressBar.classList.add('hide');
    event.target.removeEventListener('progress', onProgress);
  } else {
    progressBar.classList.remove('hide');
  }
};

const createFacilities = (key) => {
  var obj = cabinInfo[key];
  for (var prop in obj) {
      if (!obj.hasOwnProperty(prop)) continue;

      // console.log(obj[prop].image);
      if(obj[prop].value != 0) {
        var divEl = document.createElement("div");
        var imgEl = document.createElement("img");
        imgEl.src = obj[prop].image;
        var titleEl = prop == 'Guest' ? 'Max. ' + obj[prop].value + " Guests"  : (prop.toString().charAt(0).toUpperCase() + prop.slice(1)).replace("_", " ");
        divEl.appendChild(imgEl)
        divEl.appendChild(document.createTextNode(titleEl));
        document.getElementById('menu-info').appendChild(divEl);
      }
  }
}

modelViewer.addEventListener('progress', onProgress);

if(cabinType != null){
  $(".menu-title").text(cabinType.replace("-", " "));
  if(cabinType != 'Self-Photo-Studio'){
    $(".button.panoramic").css("display", "none");
    createFacilities(cabinType)
  } else {
    $(".button.panoramic").css("display", "block");
    $(".menu-title").text("Capture Your Sunshine Village Memories with Arthology's Exclusive Self-Photo Studio!");
    $(".menu-info").text("Arthology provides a special spot for guests who want to capture their moments at Sunshine Village. With easy-to-use, top-notch cameras and a lovely backdrop, your memories will be picture-perfect.");
  }
  modelViewer.setAttribute('poster','assets/images/'+cabinType+'-thumb.webp')
  modelViewer.setAttribute('src','assets/models/'+cabinType+'.glb')
  modelViewer.setAttribute('ios-src','assets/models/'+cabinType+'.usdz')
}else{
  $(".menu-title").text("Family Cabin")
  createFacilities("Family-Cabin")
  modelViewer.setAttribute('poster','assets/images/Family-Cabin-thumb.webp')
  modelViewer.setAttribute('src','assets/models/Family-Cabin.glb')
  modelViewer.setAttribute('ios-src','assets/models/Family-Cabin.usdz')
}

$("#collapsible").click(function(){
  $(".c-menu-wrapper").slideToggle("slow");
}, function(){
  $(".c-menu-wrapper").slideToggle("slow");
});